import ballerina/graphql;

public type Covid_19 record {|
    int tests;
    int date;
    string city;
    string region;
    int confirmed_cases;
    int deaths;
    int recoveries;
|};

service /graphql on new graphql:Listener(8080) {
    private Covid_19  profile;

 function init() {
        self.profile = { tests: 1200, date: 12/09/2021, city: "Windhoek",
        region:"Khomas" };
    }
    resource function get profile() returns  Covid_19 {
        return self.profile;
    }

    remote function updateName(string tested) returns Covid_19 {
        self.profile.tests = tests;
        return self.profile;
    }

    remote function updateCity(string city) returns Covid_19  {
        self.profile.city = city;
        return self.profile;
    }
}