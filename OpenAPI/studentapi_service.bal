import ballerina/http;
Student [] allusers=[];


listener http:Listener ep0 = new (8080, config = {host: "localhost"});

service /vle/api/v1 on ep0 {
    resource function get students() returns Student[]|http:Response {
return allusers;
    }
    resource function post students(@http:Payload Student payload) returns CreatedInlineResponse201|http:Response {
    allusers.push(payload);
    InlineResponse201 y={userid: "1"};
    CreatedInlineResponse201 r={body:y};
    return r;
    }

    resource function get students/[string studentnumber]() returns Student|http:Response {
       Student thatUser={};
           foreach var item in allusers {
        if(item.studentnumber==studentnumber){

         thatUser=item;
        }
           }
           return thatUser;

    }
    resource function put students/[string studentnumber](@http:Payload Student payload) returns Student|http:Response {
    
    http:Response r=new ;
    return r;
    }
    
    resource function delete students/[string studentnumber]() returns http:NoContent|http:Response {
    http:Response r=new ;
    return r;
    
    
    }
}
