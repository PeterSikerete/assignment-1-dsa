import ballerina/http;

public type CreatedInlineResponse201 record {|
    *http:Created;
    InlineResponse201 body;
|};

public type InlineResponse201 record {
    # the studentnumber of the students newly created
    string userid?;
};

public type Error record {
    string errorType?;
    string message?;
};

public type Student record {
    # the student number of the student
    string studentnumber?;
    # the first name of the student
    string firstname?;
    # the last name of the student
    string lastname?;
    # the email address of the student
    string email?;
    # the coursedetails of the student
    string coursedetails?;
    # the code to get student coursedetails
    string coursecode?;
    # the type of work in the course
    string assessment?;
    # the marks awarded for assesment
    string assessmentmark?;
};
