import ballerina/grpc;

listener grpc:Listener ep = new (9090);

@grpc:Descriptor {value: GRPC_DESC}
service "users" on ep {

    remote function create_user(CreateRequest value) returns CreateResponse|error {
    }
    remote function update_user(CreateRequest value) returns SingleUserResponse|error {
    }
    remote function view_user(string value) returns SingleUserResponse|error {
    }
    remote function delete_user(string value) returns error? {
    }
    remote function get_all_users() returns stream<SingleUserResponse, error?>|error {
    }
    remote function get_some_users(stream<string, grpc:Error?> clientStream) returns stream<SingleUserResponse, error?>|error {
    }
}

