import ballerina/io;

usersClient ep = check new ("http://localhost:9090");

public function main() returns error? {
    CreateRequest create_userRequest = {username: "ballerina", lastname: "ballerina", firstname: "ballerina", email: "ballerina", course: "ballerina", assignments: "ballerina", marks: "ballerina"};
    CreateResponse create_userResponse = check ep->create_user(create_userRequest);
    io:println(create_userResponse);

    CreateRequest update_userRequest = {username: "ballerina", lastname: "ballerina", firstname: "ballerina", email: "ballerina", course: "ballerina", assignments: "ballerina", marks: "ballerina"};
    SingleUserResponse update_userResponse = check ep->update_user(update_userRequest);
    io:println(update_userResponse);

    string view_userRequest = "ballerina";
    SingleUserResponse view_userResponse = check ep->view_user(view_userRequest);
    io:println(view_userResponse);

    string delete_userRequest = "ballerina";
    check ep->delete_user(delete_userRequest);
    stream<

SingleUserResponse, error?> get_all_usersResponse = check ep->get_all_users();
    check get_all_usersResponse.forEach(function(SingleUserResponse value) {
        io:println(value);
    });

    string get_some_usersRequest = "ballerina";
    Get_some_usersStreamingClient get_some_usersStreamingClient = check ep->get_some_users();
    check get_some_usersStreamingClient->sendString(get_some_usersRequest);
    check get_some_usersStreamingClient->complete();
    SingleUserResponse? get_some_usersResponse = check get_some_usersStreamingClient->receiveSingleUserResponse();
    io:println(get_some_usersResponse);
}

